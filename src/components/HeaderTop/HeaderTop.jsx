import { NavLink } from "react-router-dom";
import h from '../Main/styles/Header.module.css'


const HeaderTop = () => {
    return (

        <header class="header">
                <div className={h.header__content}>
                    <div className={h.container}>
                        <div className={h.header__content_inner}>
                            <div Name={h.header__logo}>
                                <a href="#"><img  alt="Oblaka" /></a>
                            </div>
                            <nav className={h.menu}>
                                <ul>
                                    <li><NavLink to="/center/clinic">Clinic</NavLink></li>
                                    <li><NavLink to="/center/doctor">Doctor</NavLink></li>
                                    <li><NavLink to="/center/service">Service</NavLink></li>
                                    <li><NavLink to="/center/share">Share</NavLink></li>
                                    <li><NavLink to="/center/comment">Comment</NavLink></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>


                <div className="container">

            <div className="admin__header__top">
                
                
                
                
                
            </div>


        </div>
            </header>



        

    )
}


export default HeaderTop;