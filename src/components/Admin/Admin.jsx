import CreateComment from "../Main/components/Comment/CreateComment"

// const Admin = (props) => {
//     const goToMainPage = () => {
//         props.history.push("/")
//     }

//     return (<div>
//         <CreateComment goToMainPage={goToMainPage} />
//     </div>)
// }




import React, {useState} from 'react';
import useStyles from "./AdminStyle";
import Container from "@material-ui/core/Container";
import CssBaseline from "@material-ui/core/CssBaseline/CssBaseline";
import Avatar from "@material-ui/core/Avatar";
import LockOutlinedIcon from "@material-ui/core/SvgIcon/SvgIcon";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox/Checkbox";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import Link from "@material-ui/core/Link";
import {useDispatch, useSelector} from "react-redux";
import {authenticate} from "../../store/actions/user/userAtions";
import { Redirect } from "react-router";
import { store } from "../..";

const Admin = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const user = useSelector(state => state.users.user);


    console.log(store.getState())
    
    

    const [state, setState] = useState({
        name: '',
        password: ''
    });


    const onSubmitHandler = e => {
        e.preventDefault();

        dispatch(authenticate({name: state.name, password: state.password}));
    };

    const onFieldChange = e => {
        const {name, value} = e.target;

        setState({...state, [name]: value})
    };


    

    return (
        <Container className={classes.main} component="main" maxWidth="xs">
            <CssBaseline />
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <LockOutlinedIcon /> 
                </Avatar>
                <Typography component="h1" variant="h5">
                    Sign in
                </Typography>
                <form className={classes.form} noValidate onSubmit={onSubmitHandler}>
                    <TextField
                        onChange={onFieldChange}
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="name"
                        label="name"
                        name="name"
                        autoFocus
                    />
                    <TextField
                        onChange={onFieldChange}
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        name="password"
                        label="Password"
                        type="password"
                        id="password"
                        autoComplete="current-password"
                    />
                    <FormControlLabel
                        control={<Checkbox value="remember" color="primary" />}
                        label="Remember me"
                    />
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                    >
                        Sign In
                    </Button>
                    <Grid container>
                        <Grid item xs>
                            <Link href="#" variant="body2">
                                Forgot password?
                            </Link>
                        </Grid>
                        <Grid item>
                            <Link href="#" variant="body2">
                                {"Don't have an account? Sign Up"}
                            </Link>
                        </Grid>
                    </Grid>
                </form>
            </div>
        </Container>
    );
};

export default Admin;
