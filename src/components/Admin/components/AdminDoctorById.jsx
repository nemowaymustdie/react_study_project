import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux"
import { getDoctorById } from "../../../store/actions/doctorActions";
import { NavLink } from "react-router-dom";
import HeaderTop from '../../HeaderTop/HeaderTop'

const AdminDoctorById = (props) => {
    const dispatch = useDispatch();
    const doctorById = useSelector(state => state.doctors.doctor)


    useEffect(() => {
        dispatch(getDoctorById(props.match.params.id))
    }, [dispatch])



    console.log(doctorById)





    return (
        <div>
            <HeaderTop />
            <div className="container">
                <img src="/images/nastya.jpg" height="100px" alt="" />
                <div>{doctorById.name}</div>
                <div>{doctorById.description}</div>
                <NavLink to={'/center/doctor/change/' + props.match.params.id}>Change</NavLink>
            </div>
        </div>

    )
}

export default AdminDoctorById;