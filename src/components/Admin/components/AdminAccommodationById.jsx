import { useEffect } from "react"
import { useDispatch, useSelector } from "react-redux"
import { getAccommodationActionById } from "../../../store/actions/accommodationActions"
import HeaderTop from '../../HeaderTop/HeaderTop'

const AdminAccommodationById = (props) => {
    const dispatch = useDispatch()
    const data = useSelector(state => state.accommadations.accommodation)

    useEffect(() => {
        dispatch(getAccommodationActionById(props.match.params.id))
    },[dispatch])

    console.log(data)

    return(
        <div>
            <HeaderTop />
            {data.text}

        </div>
    )
}

export default AdminAccommodationById;