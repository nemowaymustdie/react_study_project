import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import config from "../../../data/config";
import { getDoctorImage } from "../../../store/actions/doctorActions";


const AdminImage = () => {

    const dispatch = useDispatch();
    const data = useSelector(state => state.doctors.doctorImage)

    console.log(data)
    useEffect(() => {
        dispatch(getDoctorImage())
    }, [dispatch])

    return(
        <div>
            <img src={config.host + "/doctorImages/6049101db2ddec450304b5af"} />
        </div>

    )
}

export default AdminImage;