import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux"
import { deleteDoctorById, getDoctor } from "../../../store/actions/doctorActions";
import { NavLink } from "react-router-dom";
import Button from "@material-ui/core/Button";
import HeaderTop from '../../HeaderTop/HeaderTop'
import config from "../../../data/config";
import '../styles/AdminDoctor.css'

const AdminDoctor = () => {
    const dispatch = useDispatch();
    const doctorData = useSelector(state => state.doctors.doctors)



    const dltDoctor = (e, id) => {
        e.preventDefault();
    
        dispatch(deleteDoctorById(id))
    }


    const doctorElement = doctorData.map(el => {
        return (
            <div key={el.id}  className="center__doctor">
                <NavLink to={'/center/doctor/' + el.id}>
                    <img src={el.photoId ? config.host + `/doctorImages/${el.photoId}`: "/images/nastya.jpg"} height="100px" alt={el.name} />
                    <div>
                        {el.name}
                    </div>
                    <div className="btn">
                        <Button onClick={(e) => dltDoctor(e, el.id)}>Delete</Button>
                    </div>
                </NavLink>
            </div>
        )
    })


    useEffect(() => {
        dispatch(getDoctor())
    }, [dispatch])


    return (
        <div>
            <HeaderTop />
            <div className="container" >
                <div className="admin__doctor">
                {doctorElement}
                </div>
            </div>
        </div>
    )
}

export default AdminDoctor