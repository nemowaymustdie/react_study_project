import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getComment } from "../../../store/actions/comment/commentActions";
import HeaderTop from '../../HeaderTop/HeaderTop'

const AdminComment = () => {

    const dispatch = useDispatch();
    const doctorData = useSelector(state => state)

    console.log(doctorData)
    

    useEffect(() => {
        dispatch(getComment())
    }, [dispatch])
    return(
        <div>
            <HeaderTop />
            AdminComment
        </div>
    )
}

export default AdminComment;