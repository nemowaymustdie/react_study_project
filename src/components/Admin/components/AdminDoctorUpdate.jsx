import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getDoctorById, updateDoctorById } from "../../../store/actions/doctorActions";
import HeaderTop from '../../HeaderTop/HeaderTop'


const AdminDoctorUpdate = (props) => {
    const dispatch = useDispatch();
    const doctorById = useSelector(state => state.doctors.doctor);

    useEffect(() => {
        dispatch(getDoctorById(props.match.params.id))
    }, [dispatch])

    const [name, setName] = useState(doctorById.name);
    const [description, setDescription] = useState(doctorById.description)
    const [file, setFile] = useState(null);

    const onFileChangeHandler = e => {
        setFile(e.target.files[0])
    };

    const onSubmitHandler = (e) => {
        e.preventDefault();
        let doctor = {
            id: doctorById.id,
            name: name,
            description: description,
            photoId: doctorById.photoId,
            position: doctorById.position
        }

        const imageData = new FormData();

        imageData.append("file", file);






        dispatch(updateDoctorById(doctorById.id, doctor, imageData))
    }


    return (
        <div>
            <HeaderTop />
            <div className="container">
                <form onSubmit={onSubmitHandler}>
                    <input type="file" accept=".jpg, .jpeg, .png" onChange={onFileChangeHandler} />
                    <input type="text" value={name} onChange={(e) => setName(e.target.value)} />
                    <input type="textarea" value={description} onChange={(e) => setDescription(e.target.value)} />
                    <button>Submit</button>
                </form>
            </div>
        </div>

    )
}

export default AdminDoctorUpdate;