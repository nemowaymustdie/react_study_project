import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux"
import { NavLink } from "react-router-dom";
import { getServiceById } from "../../../store/actions/serviceActions";
import HeaderTop from '../../HeaderTop/HeaderTop'

const AdminServiceById = (props) => {
    const dispatch = useDispatch();
    const data = useSelector(state => state.services.service)

    console.log(data.branch)

    const branches = data.branch.map(el => {
        return (
            <NavLink to={`/center/branch/${el.id}`}>
                {el.title}
            </NavLink>

        )
    })



    console.log(data)

    useEffect(() => {
        dispatch(getServiceById(props.match.params.id))
    }, [dispatch])

    return (
        <div>
            <HeaderTop />
            <div className="container">
                {data.title}
                {branches}

            </div>
        </div>
    )
}

export default AdminServiceById;