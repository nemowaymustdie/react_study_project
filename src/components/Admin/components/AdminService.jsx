import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { getService } from "../../../store/actions/serviceActions";
import HeaderTop from '../../HeaderTop/HeaderTop'

const AdminService = () => {
    const dispatch = useDispatch();
    const data = useSelector(state => state.services.services)

    useEffect(() => {
        dispatch(getService())
    }, [dispatch])


    const serviceElement = data.map(el => {
        return (
            <div>
                <NavLink to={`/center/service/${el.id}`}>{el.title}</NavLink>

            </div>
        )
    })

    return (
        <div>
            <HeaderTop />
            <div className="container">
                {serviceElement}
            </div>
        </div>


    )
}


export default AdminService;