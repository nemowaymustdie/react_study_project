import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux"
import { NavLink } from "react-router-dom";
import { getBranchById } from "../../../store/actions/branchActions";
import HeaderTop from '../../HeaderTop/HeaderTop'


const AdminBranchById = (props) => {
    const dispatch = useDispatch();
    const data = useSelector(state => state.branches.branch)

    console.log(data)
    useEffect(() => {
        dispatch(getBranchById(props.match.params.id))
    }, [dispatch])

    const accommodationElement = data.accommodations.map(el => {
        return(
            <NavLink to={`/center/accommodation/${el.id}`}>
                {el.text}
            </NavLink>
        )
    })
    


    return(
        <div className="container">
            <HeaderTop />
            {accommodationElement}
        </div>
    )

}

export default AdminBranchById;