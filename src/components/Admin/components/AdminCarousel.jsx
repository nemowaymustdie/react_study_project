// Import Swiper React components
import { Swiper, SwiperSlide } from 'swiper/react';
import CarouselSlide from '../../Carousel/CarouselSlide'
import SwiperCore, { Navigation, Pagination, Scrollbar, A11y } from 'swiper';

// Import Swiper styles
import 'swiper/swiper.scss';




import 'swiper/swiper.scss';
import '../styles/AdminCarousel.scss';


import '../../Carousel/CarouselSlide.css'

SwiperCore.use([Navigation, Pagination, Scrollbar, A11y]);

const AdminCarousel = (props) => {




    const slide = props.slides.map(el => {
        return (
            <SwiperSlide>
                <div class="shares__slider border-radius">
                    <div class="shares__slider-inner">
                        <CarouselSlide slide={el} />
                    </div>
                </div>
            </SwiperSlide>
        )
    })

    return (
        <div className="container">
            <Swiper
                spaceBetween={500}
                slidesPerView={1}
                navigation
                onSlideChange={() => console.log('slide change')}
                onSwiper={(swiper) => console.log(swiper)}
            >
                <div className="slide__slide">
                    {slide}
                </div>
            </Swiper>
        </div>
    );
};


export default AdminCarousel;