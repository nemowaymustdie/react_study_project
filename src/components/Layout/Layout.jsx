import React from 'react';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Admin from '../Admin/Admin';
import Main from '../Main/Main';
import Comment from '../Main/components/Comment/Comment';
import AdminClinic from '../Admin/components/AdminClinic';
import AdminComment from '../Admin/components/AdminComment';
import AdminDoctor from '../Admin/components/AdminDoctor';
import AdminShare from '../Admin/components/AdminShare';
import AdminService from '../Admin/components/AdminService';
import AdminDoctorById from '../Admin/components/AdminDoctorById';
import AdminDoctorUpdate from '../Admin/components/AdminDoctorUpdate';
import AdminServiceById from '../Admin/components/AdminServiceById';
import AdminBranchById from '../Admin/components/AdminBranchById';
import AdminAccommodationById from '../Admin/components/AdminAccommodationById';
import HeaderTop from '../HeaderTop/HeaderTop';




const Layout = () => {
    return (
        <React.Fragment>
            <Switch>
                <Route path="/" exact component={Main} />
                <Route path="/admin" exact component={Admin} />
                <Route path="/comment/:id" component={Comment} />

                
                <Route path="/center/clinic" exact component={AdminClinic} />
                <Route path="/center/comment" exact component={AdminComment} />
                <Route path="/center/doctor" exact component={AdminDoctor} />
                <Route path="/center/share" exact component={AdminShare} />
                <Route path="/center/service" exact component={AdminService} />

                <Route path="/center/doctor/:id" exact component={AdminDoctorById} />
                <Route path="/center/doctor/change/:id" exact component={AdminDoctorUpdate} />
                <Route path="/center/service/:id" exact component={AdminServiceById} />
                <Route path="/center/branch/:id" exact component={AdminBranchById} />
                <Route path="/center/accommodation/:id" exact component={AdminAccommodationById} />
            </Switch>
        </React.Fragment>
    )
}

export default Layout;