import React, { Component } from 'react';
import './CarouselSliders.css'


const CarouselLeftArrow = (props) => {
    return (
      <a
        href="#"
        className="carousel__arrow carousel__arrow--left"
        onClick={props.onClick}
      >
        <span className="fa fa-2x fa-angle-left" >Left</span>
      </a>
    );
}

const CarouselRightArrow = (props) => {
    return (
      <a
        href="#"
        className="carousel__arrow carousel__arrow--right"
        onClick={props.onClick}
      >
        <span className="fa fa-2x fa-angle-right">Right</span>
      </a>
    );
}



class CarouselSliders extends Component {
    
    render() {
        console.log(this.props)
        return (
        <li
          className={
            this.props.index == this.props.activeIndex
              ? "carousel__slide carousel__slide--active"
              : "carousel__slide"
          }
        >
          <div class="news__slider-item">
                                    <div class="news__slider-title">
                                        {this.props.slide.name}
                  </div>
                                    <div class="news__slider-text">
                                        {this.props.slide.text}
                  </div>
                                    <div class="news__slider-author">
                                        Пациент стоматологической клиники "Облака"
                  </div>
                                </div>
        </li>
      );
    }
  }


class CommentCarousel extends Component {
    constructor(props) {
        super(props);

        // this.goToSlide = this.goToSlide.bind(this);
        this.goToPrevSlide = this.goToPrevSlide.bind(this);
        this.goToNextSlide = this.goToNextSlide.bind(this);

        this.state = {
            activeIndex: 0
        };
    }

    // goToSlide(index) {
    //   this.setState({
    //     activeIndex: index
    //   });
    // }

    goToPrevSlide(e) {
        e.preventDefault();

        let index = this.state.activeIndex;
        let { slides } = this.props;
        let slidesLength = slides.length;

        if (index < 1) {
            index = slidesLength;
        }

        --index;

        this.setState({
            activeIndex: index
        });
    }

    goToNextSlide(e) {
        e.preventDefault();

        let index = this.state.activeIndex;
        let { slides } = this.props;
        let slidesLength = slides.length - 1;

        if (index === slidesLength) {
            index = -1;
        }

        ++index;

        this.setState({
            activeIndex: index
        });
    }

    render() {
        return (
            <div className="carousel">
                <CarouselLeftArrow onClick={e => this.goToPrevSlide(e)} />

                <ul className="carousel__slides">
                    {this.props.slides.map((slide, index) =>
                        <CarouselSliders
                            key={index}
                            index={index}
                            activeIndex={this.state.activeIndex}
                            slide={slide}
                        />
                    )}
                </ul>

                <CarouselRightArrow onClick={e => this.goToNextSlide(e)} />

                {/* <ul className="carousel__indicators">
            {this.props.slides.map((slide, index) =>
              <CarouselIndicator
                key={index}
                index={index}
                activeIndex={this.state.activeIndex}
                isActive={this.state.activeIndex==index} 
                onClick={e => this.goToSlide(index)}
              />
            )}
          </ul> */}
            </div>
        );
    }
}

export default CommentCarousel;