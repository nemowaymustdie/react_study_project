import d from '../styles/DoctorCard.module.css'
import image from '../images/nastya.jpg'
import Doctor from './Doctor'
import config from '../../../data/config'

const DoctorCard = (props) => {

    return(
        <div class={d.services__item}>
            
            <img src={props.doctor.photoId ? config.host + `/doctorImages/${props.doctor.photoId}`: "/images/nastya.jpg"} alt="" />
            <div class={d.services__item__title}>
            {props.doctor.name}
            </div>
            <div class={d.services__item__text}>
            {props.doctor.description}
            </div>
        </div>
    )
}

export default DoctorCard;