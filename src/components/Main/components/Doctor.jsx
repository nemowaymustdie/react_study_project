import DoctorCard from "./DoctorCard"
import d from '../styles/Doctor.module.css'

const Doctor = (props) => {
    const doctorElement = props.doctor.map(el => <DoctorCard doctor={el}/>)
    return (
        <div className={d.doctors__page} id="clinic__doctors">
            <div className={d.container}>
                <section className={d.services}>

                <div className={d.services__top}>
                    <div className={d.services__title__box}>
                        <div className={d.services__title}>
                            Наши специалисты
                        </div>
                    </div>
                    <div class={d.services__btn}>
                        <a href="#">Показать всех врачей</a>
                    </div>
                </div>

                <div class={d.services__items}>
                    {doctorElement}
                </div>
                    
                </section>
            </div>
        </div>

    )
}

export default Doctor;