import React, {useState} from 'react';
import {useDispatch} from "react-redux";
import {createComment} from "../../../../store/actions/comment/commentActions";
import Container from "@material-ui/core/Container";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import SendIcon from "@material-ui/icons/Send";
import { Redirect } from 'react-router';


const CreateComment = (props) => {
    
    const dispatch = useDispatch();
    const [name, setName] = useState('');
    const [text, setText] = useState('');
    const [redirect, setRedirect] = useState(false);


    const onSubmitHandler = e => {
        e.preventDefault();

        const comment = {
            name,
            text
        }

        setName('')
        setText('')

        dispatch(createComment(comment)).then(e => {
            // setRedirect(true)
            props.goToMainPage();
        });
    };

    // if(redirect) return <Redirect to='/'/>


    return (
        <React.Fragment>
            <Container>
                <form onSubmit={onSubmitHandler}>
                    <TextField
                        fullWidth
                        margin="normal"
                        type="text"
                        name="name"
                        value={name}
                        onChange={(e) => setName(e.target.value)}
                    />
                    <TextField
                        fullWidth
                        margin="normal"
                        type="text"
                        name="text"
                        value={text}
                        onChange={(e) => setText(e.target.value)}
                    />
                    <Button
                        variant="contained"
                        color="primary"
                        type="submit"
                        startIcon={<SendIcon />}
                    >
                        Send
                    </Button>
                </form>
            </Container>
        </React.Fragment>
    );
};


export default CreateComment; 