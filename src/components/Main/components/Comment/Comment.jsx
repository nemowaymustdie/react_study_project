import {useDispatch, useSelector} from "react-redux";
import { getCommentById } from "../../../../store/actions/comment/commentActions";
import {useEffect} from 'react'; 

const Comment = (props) => {

    const dispatch = useDispatch();
    const clinicData = useSelector(state => state.comments.comment);
    

    useEffect(() => {
        dispatch(getCommentById(props.match.params.id))
    }, [dispatch]);

    console.log(clinicData)
    
    return(
        <div>
            <h3>haha {clinicData.id}</h3>  
        </div>
    )
}

export default Comment;