import c from '../styles/Clinic.module.css'
import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import { getCommon } from '../../../store/actions/commonActions'

const Clinic = (props) => {

    


    const clinicElement = props.clinic.map(el => {
        return (
            <div>
                <div class={c.about__clinic} id="about__clinic">
                    <div class={c.container}>
                        <div class={c.main__inner}>

                            <div class={c.clinic__video}>
                                <iframe width="560" height="315" src={el.video}
                                    frameborder="0"
                                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                    allowfullscreen></iframe>

                            </div>
                            <div class={c.clinic__info}>
                                <div class={c.clinic__title}>стоматология "{el.title}"</div>
                                <div class={c.clinic__description}>{el.description}</div>
                                <a data-fancybox data-src="#modal" href="javascript:;" class={c.clinic__more} href="#">Подборнее</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    });

    return (
        <div>
            {clinicElement} 
        </div>
    )

}


export default Clinic;