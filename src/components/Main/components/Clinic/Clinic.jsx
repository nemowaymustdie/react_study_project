import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import { getComment, deleteComment, getCommentById } from "../../../../store/actions/comment/commentActions";
import Button from "@material-ui/core/Button";
import {NavLink} from "react-router-dom";


const Clinic = () => {

    const dispatch = useDispatch();
    const clinicData = useSelector(state => state.comments);

    useEffect(() => {
        dispatch(getComment())
    }, [dispatch]);

    console.log(clinicData)


  const dltComment = (e, id) => {
    e.preventDefault();

    dispatch(deleteComment(id))
  }


  const comment = clinicData.clinicsList.map(c => {
    return(
      <div key={c.id} className={c.name}>
        <NavLink to={'/comment/' + c.id}>{c.name}</NavLink> 
        <Button onClick={(e) => dltComment(e, c.id)}>Delete</Button>
      </div>
    )
  });


  // onClick={() => {
  //   dispatch(getCommentById(c.id)).then(e => {props.goToCommentId(c.id)})


    return(
        <div className="about__clinic">
          {comment}

          {/* {clinicData.name} */}
            {/* <div className="container">
            <div className="main-inner">
              <div className="clinic__video">
                Video
              </div>
              <div className="clinic__info">
                <div className="clinic__title">стоматология "Облака"</div>
                <div className="clinic__description">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Distinctio obcaecati commodi architecto perspiciatis quaerat explicabo repellat maxime id non deleniti. Libero sed, non iure sequi eos suscipit quis vero a?</div>
                <a data-fancybox data-src="#modal" className="clinic__more">Подборнее</a>
              </div>
            </div>
          </div> */}
        </div>
    )
}

export default Clinic;