import useStyles from '../styles/HeaderStyle'
import h from '../styles/Header.module.css'
import logoOblako from '../images/logo.png'


const Header = (props) => {


    const hElement = props.contacts.map(el => {
        return(
            <div>
            <div className={h.header__top}>
                <div className={h.container}>
                    <div class="header__contacts">
                        <a className={h.header__phone} href={el.telForCall}>{el.number}</a>
                        <a className={h.header__email} href="#">{el.email}</a>
                        <a className={h.header__btn} href="#">Оставить отзыв</a>
                    </div>
                </div>
            </div>


            <header class="header">
                <div className={h.header__content}>
                    <div className={h.container}>
                        <div className={h.header__content_inner}>
                            <div Name={h.header__logo}>
                                <a href="#"><img src={logoOblako} alt="Oblaka" /></a>
                            </div>
                            <nav className={h.menu}>
                                <ul>
                                    <li><a class="clinic__button" href="#about__clinic">О клинике</a></li>
                                    <li><a class="doctor__button" href="#clinic__doctors">Врачи</a></li>
                                    <li><a class="shares__button" href="#clinic__shares">Акции</a></li>
                                    <li><a class="attendance__button" href="#clinic__attendance">Услуги</a></li>
                                    <li><a class="reviews__button" href="#clinic__reviews">Отзывы</a></li>
                                    <li><a class="contacts__button" href="#footer">Контакты</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </header>
        </div>
        )
    })

    return (
        <div>
            {hElement}
        </div>
        
    )
}

export default Header;