

const Contacts = (props) => {


    const hElement = props.contacts.map(el => {
        return (
            <footer class="footer" id="footer">
                <div class="footer__content">
                    <div class="container">
                        <div class="footer__inner">
                            <div class="footer__info">
                                <div class="footer__title">

                                </div>
                                <div class="footer__text">

                                </div>

                                <ul class="footer__list">
                                    <li><a class="footer__phone" href={el.telForCall}>{el.number}</a></li>
                                    <li><a href="#">{el.email}</a></li>
                                    <li><a class="footer__adress" href="#">{el.address}</a></li>
                                </ul>
                            </div>
                            <div class="footer__map">
                                

                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer__copy">
                    <div class="container">
                        <div class="copy__text">
                            © 2021 Стоматологическая клиника Облака.
        </div>
                    </div>
                </div>

            </footer>
        )
    })



    return (
        <div>
            {hElement}
        </div>
    )
}


export default Contacts;