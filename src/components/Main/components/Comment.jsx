import '../styles/Comment.css'
import '../styles/CommentSlide.scss'
import 'swiper/components/pagination/pagination.scss';



import { Swiper, SwiperSlide } from 'swiper/react';

import SwiperCore, { Navigation, Pagination, Autoplay } from 'swiper';





import 'swiper/swiper.scss';


SwiperCore.use([Navigation, Pagination, Autoplay]);



const Comment = (props) => {

  const comment = props.comment.map(el => {
    return (
      <div>
        <SwiperSlide>
        <div class="news__inner">
          
          <div class="news__slider">
            <div class="news__slider-inner">
              <div class="news__slider-item">
                <div class="news__slider-title">
                  {el.name}
                </div>
                <div class="news__slider-text">
                  {el.text}
                </div>
                <div class="news__slider-author">
                  Пациент стоматологической клиники "Облака"
                  </div>
              </div>
            </div>
          </div>
        
          </div>
        </SwiperSlide>
      </div>

    )
  })

  return (
    <div class="clinic__reviews" id="clinic__reviews">
      <section class="news">
        <div class="container">
          <div class="news__top">
            <div class="news__title-box">
              <div class="news__title">
                Отзывы стоматологии
              </div>
            </div>
          </div>
          


          <div className="comment__swiper">
            <Swiper
              spaceBetween={50}
              slidesPerView={1}
              pagination={{ clickable: true }}
              autoplay
              onSlideChange={() => console.log('slide change')}
              onSwiper={(swiper) => console.log(swiper)}
            >
              
              {comment}

            </Swiper>

          </div>






          
        </div>
      </section>
    </div>
  )
}

export default Comment;