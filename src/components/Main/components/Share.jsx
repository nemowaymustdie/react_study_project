import $ from 'jquery';
import AdminCarousel from '../../Admin/components/AdminCarousel';
import Carousel from '../../Carousel/Carousel';

import '../styles/Share.css'



//   const Carousel = () => {


//     return (
//         <div>
//             <div class="shares__title title">

//             </div>
//             <div class="shares__text">
//                 <div class="shares__description">

//                 </div>

//                 <div class="shares__price">

//                 </div>
//             </div>
//             <div class="shares__important">
//             </div>
//         </div>
//     )
// }



const Share = (props) => {
  
    return (

        <section class="news" id="clinic__shares">
        <div class="container">
          <div class="news__top">
            <div class="news__title-box">
              <div class="news__title">
                Акции стоматологии
              </div>
            </div>
          </div>
          <div class="shares__inner">
            {/* <div class="shares__slider border-radius">
              <div class="shares__slider-inner"> */}
                
                  <AdminCarousel slides={props.share}/>
              {/* </div>
            </div> */}
          </div>
        </div>
      </section>

        
        // <div class="clinic__shares" id="clinic__shares">
        //     <section class="news">
        //         <div class="container">
        //             <div class="news__top">
        //                 <div class="news__title-box">
        //                     <div class="news__title">
        //                         Акции стоматологии
        //       </div>
        //                 </div>
        //             </div>
        //             <div class="shares__inner">
        //                 <div class="shares__slider border-radius">
        //                     <div class="shares__slider-inner">


        //                         <div class="news__slider-item">
        //                             <div class="shares__title title">
        //                                 Профессиональная гигиена полости рта
        //           </div>
        //                             <div class="shares__text">
        //                                 <div class="shares__description">
        //                                     <ul>
        //                                         <li>проводиться врачами-терапевтами</li>
        //                                         <li>на оборудовании европейских стандартов(KaVa, Германия)</li>
        //                                         <li>строго калиброванным порошком с мягким гранулами</li>
        //                                         <li>они бережно очищают налет, не царапая эмаль</li>
        //                                     </ul>
        //                                 </div>

        //                                 <div class="shares__price">
        //                                     Стоимость: 2000-2500 сом
        //             </div>
        //                             </div>
        //                             <div class="shares__important">
        //                                 Гарантия сохранения здоровья зубов и целостности эмали
        //           </div>
        //                         </div>




        //                     </div>
        //                 </div>
        //             </div>
        //         </div>
        //     </section>
        // </div>
    )
}




// $(function () {

//     $('.shares__slider-inner').slick({
//       nextArrow: '<button type="button" class="shares-btn slick-next"></button>',
//       prevArrow: '<button type="button" class="shares-btn slick-prev"></button>',
//       infinite: false
//     });

//     $('select').styler();

//     $('.header__btn-menu').on('click', function () {
//       $('.menu ul').slideToggle();
//     });

// });


export default Share;