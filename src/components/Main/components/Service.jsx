import Accordion from '../../Accordion/Accordion'

const Service = (props) => {

    
    
    return(
        <div id="clinic__attendance">
            <div className="container">
            <div class="news__top">
            <div class="news__title-box">
              <div class="news__title">
                Услуги стоматологии
              </div>
            </div>
          </div>
            </div>
    
            <Accordion panels={ props.service }/>
        </div>
    )
}

export default Service;