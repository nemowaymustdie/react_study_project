import { makeStyles } from "@material-ui/core/styles";


const useStyles = makeStyles((theme) => ({
    container : {
        maxWidth: '1170px',
        margin: '0 auto',
        padding: '0 15px',
    },
    header__top: {
        backgroundColor: '#1E2019'
    }
}));

export default useStyles;