import Header from './components/Header'
import Clinic from './components/Clinic'
import Doctor from './components/Doctor'
import Share from './components/Share'
import Service from './components/Service'
import Comment from './components/Comment'
import Contacts from './components/Contacts'
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from "react-redux";
import { getCommon } from '../../store/actions/commonActions'

const Main = () => {
    const dispatch = useDispatch();
    const data = useSelector(state => state.commonReducer.commons);



    useEffect(() => {
        dispatch(getCommon())
    }, [dispatch]);



    const element = (
        <div>
            <Header contacts={data.clinic} />
            <Clinic clinic={data.clinic} />
            <Doctor doctor={data.doctor} />
            <Share share={data.share} />
            <Service service={data.department} />
            <Comment comment={data.comment} />
            <Contacts contacts={data.clinic} />
        </div>
    )


    return (
        <div>
            {element}
        </div>
    )
}

export default Main;