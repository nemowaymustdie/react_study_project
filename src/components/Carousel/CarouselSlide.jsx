import './CarouselSlide.css'

const CarouselSlide = (props) => {
    console.log(props)
    
    const li = props.slide.descriptions.map(el => {
        return(
            <li>
                {el}
            </li>

        )
    })
    return (
        <div className="news__slider-item">
            <div className="shares__title title">
                {props.slide.title}
            </div>
            <div className="shares__text">
                <div className="shares__description">
                    <ul>
                        {li}
                    </ul>

                    </div>

                <div className="shares__price">
                    Стоимость: {props.slide.price} сом
                    </div>
            </div>
            <div className="shares__important">
            </div>
        </div>

    );

}


export default CarouselSlide;