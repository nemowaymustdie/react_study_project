import axiosOrders from "../../data/axiosOrders";

export const GET_ACCOMMODATION_BY_ID = "GET_ACCOMMODATION_BY_ID";


const getAccommodationByIdSuccess = accommodation => ({type: GET_ACCOMMODATION_BY_ID, payload: {accommodation}})



export const getAccommodationActionById = (id) => {
    return async dispatch => {
        const accommodation = await axiosOrders.get(`/accommodation/${id}`);

        dispatch(getAccommodationByIdSuccess(accommodation.data))
    }
}