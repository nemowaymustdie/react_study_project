import axiosOrders from "../../data/axiosOrders";

export const GET_SERVICE = "GET_SERVICE";

export const GET_SERVICE_BY_ID = "GET_SERVICE_BY_ID";


const getServiceSuccess = service =>  ({type: GET_SERVICE, payload: {service}})

const getServiceByIdSuccess = service => ({type: GET_SERVICE_BY_ID, payload: { service }})

export const getService = () => {
    return async dispatch => {
        const service = await axiosOrders.get('/department');

        dispatch(getServiceSuccess(service.data))
    }
}

export const getServiceById = (id) => {
    return async dispatch => {
        const service = await axiosOrders.get(`/department/${id}`);

        dispatch(getServiceByIdSuccess(service.data))
    }
}