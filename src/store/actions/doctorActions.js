import axiosOrders from "../../data/axiosOrders";

export const GET_DOCTOR = "GET_DOCTOR"
export const GET_DOCTOR_BY_ID = "GET_DOCTOR_BY_ID"
export const GET_DOCTOR_IMAGE = "GET_DOCTOR_IMAGE"

const getDoctorSuccess = doctor => ({
    type: GET_DOCTOR,
    payload: {
        doctor
    }
})

const getDoctorImageSuccess = doctorImage => ({
    type: GET_DOCTOR_IMAGE,
    payload: {
        doctorImage
    }
})

const getDoctorByIdSuccess = doctor => ({
    type: GET_DOCTOR_BY_ID,
    payload: {
        doctor
    }
})


export const getDoctor = () => {
    return async dispatch => {


        const doctor = await axiosOrders.get('/doctor')
        console.log(doctor)

        dispatch(getDoctorSuccess(doctor.data))
    }
}

export const getDoctorImage = () => {
    return async dispatch => {


        const doctor = await axiosOrders.get('/doctorImages/604906f488609e46759b155b', {
            headers: {
                'Content-Type': 'image/jpeg'
            }
        })

        dispatch(getDoctorImageSuccess(doctor.data))
    }
}



export const getDoctorById = (id) => {
    return async dispatch => {
        const doctor = await axiosOrders.get(`/doctor/${id}`)


        dispatch(getDoctorByIdSuccess(doctor.data))
    }
}

export const deleteDoctorById = id => {
    return async dispatch => {
    await axiosOrders.delete(`/doctor/delete/${id}`)

        dispatch(getDoctor())

    }
}

export const updateDoctorById = (id, doctorData, imageData) => {
    return async dispatch => {

        if (doctorData.photoId == null) {
            const doctorImage = await axiosOrders.put(`/doctorImages/update/id`, imageData, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            })

            doctorData.photoId = doctorImage.data.id

            const final = JSON.stringify(doctorData);
            console.log(doctorImage)

            const doctor = await axiosOrders.put(`/doctor/update/${id}`, final, {
                headers: {
                    'Content-Type': 'application/json'
                }
            })

        } else {
            const doctorImage = await axiosOrders.put(`/doctorImages/update/${doctorData.photoId}`, imageData, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            })

            doctorData.photoId = doctorImage.data.id

            const final = JSON.stringify(doctorData);
            console.log(doctorImage)

            const doctor = await axiosOrders.put(`/doctor/update/${id}`, final, {
                headers: {
                    'Content-Type': 'application/json'
                }
            })


        }

    }
}