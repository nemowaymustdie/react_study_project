import axiosOrders from "../../data/axiosOrders";

export const GET_COMMON = "GET_COMMON";

const getCommonSucces = common => ({type: GET_COMMON, payload: {common}})

export const getCommon = () => {
    return async dispatch => {
        try {
            const common = await axiosOrders.get("/common");

            console.log(common)

            dispatch(getCommonSucces(common.data));
            
        } catch(e) {
            console.log(e)
        }
    }
};