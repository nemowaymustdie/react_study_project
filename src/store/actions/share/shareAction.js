export const GET_SHARE_SUCCESS = "GET_SHARE_SUCCESS";


const getShareSuccess = share => ({type: GET_SHARE_SUCCESS, payload: {share}})


export const getShare = () => {
    return async dispatch => {
        const share = axiosOrders.get('/share');

        dispatch(getShareSuccess(share.data))
    }
}