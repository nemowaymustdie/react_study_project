import axiosOrders from "../../data/axiosOrders";


export const GET_BRANCH_BY_ID = "GET_BRANCH_BY_ID";



export const getBranchByIdSucces = branch => ({type: GET_BRANCH_BY_ID, payload: {branch}}) 


export const getBranchById = (id) => {
    return async dispatch => {
        const branch = await axiosOrders.get(`/branch/${id}`)
        console.log(branch)
        dispatch(getBranchByIdSucces(branch.data))
    }
}

