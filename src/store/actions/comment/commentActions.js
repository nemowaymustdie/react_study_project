import axiosOrders from "../../../data/axiosOrders";

export const GET_CLINIC_SUCCESS = "GET_CLINIC_SUCCESS";
export const GET_COMMENT_BY_ID = "GET_COMMENT_BY_ID";
export const POST_COMMENT = "POST_COMMENT";
export const DELETE_COMMENT = "DELETE_COMMENT";

const getCommentByIdSucces = comment => ({type: GET_COMMENT_BY_ID, payload: {comment}})

const getClinicSuccess = clinic => ({type: GET_CLINIC_SUCCESS, payload: {clinic}});

const postComment = () => ({type: POST_COMMENT})

const deleteCommentSuccess = () => ({type: DELETE_COMMENT})

export const getComment = () => {
    return async dispatch => {

        const final = localStorage.getItem('state');
        const clinic = await axiosOrders.get("/comment");


        dispatch(getClinicSuccess(clinic.data));
    }
};

export const deleteComment = (id) => {
    return async dispatch => {
        const final = localStorage.getItem('state');
        await axiosOrders.delete(`/comment/deleteComment/${id}`)
        dispatch(getComment());
    }
}


export const getCommentById = (id) => {
    return async dispatch => {
        try {
            const final = localStorage.getItem('state');
            const comment = await axiosOrders.get(`/comment/${id}`, {headers: {'Authorization': `${final}` }});

            console.log("comment has been added")

            dispatch(getCommentByIdSucces(comment.data));
        } catch (e) {
            console.log(e);
        }
    }
};


export const createComment = (postData) => {
    return async dispatch => {
        try {

            const final = JSON.stringify(postData);

            await axiosOrders.post("/comment/addComment", final, {headers: {'Content-Type': 'application/json'}});

            dispatch(postComment())

        } catch (e) {
            console.log(e);
        }
    }
};