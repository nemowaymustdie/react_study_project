import { GET_SERVICE, GET_SERVICE_BY_ID } from "../actions/serviceActions"

const initialState = {
    services: [
        {
            "id": "",
            "title": "",
            "branch": [
                {
                    "id": "",
                    "title": "",
                    "accommodations": [
                        {
                            "id": "",
                            "text": "",
                            "minPrice": null,
                            "maxPrice": null
                        }
                    ]
                }
            ]
        }
    ],
    service: {
        "id": "",
        "title": "",
        "branch": [
            {
                "id": "",
                "title": "",
                "accommodations": [
                    {
                        "id": "",
                        "text": "",
                        "minPrice": null,
                        "maxPrice": null
                    }
                ]
            }
        ]
    }
}


const serviceReducer = (state = initialState, action) => {
    switch(action.type){
        case GET_SERVICE:
            return {...state, services: action.payload.service}
        case GET_SERVICE_BY_ID:
            return {...state, service: action.payload.service }
        default:
            return state
    }
}

export default serviceReducer;