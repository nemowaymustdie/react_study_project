import {GET_COMMON} from "../actions/commonActions";

const initialState = {
    commons: {
        "clinic": [
          {
            "id": "",
            "title": "",
            "telForCall": "",
            "image": null,
            "video": "",
            "email": "",
            "address": "",
            "number": "",
            "description": null
          }
        ],
        "doctor": [
          {
            "id": "",
            "name": "",
            "photo": null,
            "description": ""
          }
        ],
        "share": [
          {
            "id": "",
            "price": null,
            "image": null,
            "title": "",
            "descriptions": [
              "",
              ""
            ]
          }
        ],
        "comment": [
          {
            "id": "",
            "name": "",
            "text": ""
          }
        ],
        "department": [
          {
            "id": "",
            "title": "",
            "branch": [
              {
                "id": "",
                "title": "",
                "accommodations": [
                  {
                    "id": "",
                    "text": "",
                    "minPrice": null,
                    "maxPrice": null
                  }
                ]
              }
            ]
          }
        ]
      }
};

const commonReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_COMMON:
            return {...state, commons: action.payload.common};
        default:
            return state;
    }
};

export default commonReducer;