import {GET_CLINIC_SUCCESS} from "../../actions/comment/commentActions"
import {GET_COMMENT_BY_ID} from "../../actions/comment/commentActions"
import {DELETE_COMMENT} from "../../actions/comment/commentActions"

const initialState = {
    clinicsList: [],
    comment: {}
}





const clinicReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_CLINIC_SUCCESS:
            return {...state, clinicsList: action.payload.clinic}
        case GET_COMMENT_BY_ID:
            return {...state, comment: action.payload.comment}
        case DELETE_COMMENT:
            return {...state}
        default:
            return state;
    }
}

export default clinicReducer