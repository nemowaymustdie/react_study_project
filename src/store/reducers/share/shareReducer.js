

const initialState = {
    shareList: [],
    share: {}
}




const ShareReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_SHARE_SUCCESS:
            return {...state, shareList: action.payload.share}
        default:
            return state;
    }
}

export default ShareReducer;
