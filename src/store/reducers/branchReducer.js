import { GET_BRANCH_BY_ID } from '../actions/branchActions'


const initialState = {
    branch: {
        "id": "",
        "title": "",
        "accommodations": [
            {
                "id": "",
                "text": "",
                "minPrice": null,
                "maxPrice": null
            }
        ]
    }
}


const branchReducer = (state = initialState, action) => {
    switch(action.type){
        case GET_BRANCH_BY_ID:
            return {...state, branch: action.payload.branch}
        default:
            return state
    }
}


export default branchReducer;