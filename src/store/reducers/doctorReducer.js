import {GET_DOCTOR, GET_DOCTOR_BY_ID, GET_DOCTOR_IMAGE} from '../actions/doctorActions'

const initialState = {
    doctors : [
        {
          "id": "",
          "name": "",
          "photo": null,
          "description": ""
        }
    ],
    doctor: {
        "id": "",
        "name": "",
        "photo": null,
        "description": ""
      },
    doctorImage: {}
    
}


const doctorReducer = (state = initialState, action) =>{
    switch(action.type){
        case GET_DOCTOR:
            return {...state, doctors: action.payload.doctor}
        case GET_DOCTOR_BY_ID:
            return {...state, doctor: action.payload.doctor}
        case GET_DOCTOR_IMAGE:
            return {...state, doctorImage: action.payload.doctorImage }
        default:
            return state
    }

}

export default doctorReducer;