import { GET_ACCOMMODATION_BY_ID } from "../actions/accommodationActions"


const initialState = {
    accommodation: {
            "id": "",
            "text": "",
            "minPrice": null,
            "maxPrice": null
        }
}

const accommodationReducer = (state = initialState, action) => {
    
    switch(action.type){
        case GET_ACCOMMODATION_BY_ID:
            return {...state, accommodation: action.payload.accommodation}
        default:
            return state
    }
}

export default accommodationReducer;