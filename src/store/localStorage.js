import {AUTHENTICATION_SUCCESS} from "./actions/user/userAtions";

const actionsList = [AUTHENTICATION_SUCCESS];

const saveToLocalStorage = state => {
    try {
        localStorage.setItem('state', JSON.stringify(state) );
    } catch (e) {
        console.log('could not save to state');
    }
};

export const loadFromLocalStorage = () => {
    try {
        const data = localStorage.getItem('state');

        if (data === null) {return undefined;}

        console.log('token loaded');
        return JSON.parse(data);
    } catch (e) {
        return undefined;
    }
};

export const localStorageMiddleware = store => next => action => {
    let result = next(action);

    if (actionsList.includes(action.type)) {
        saveToLocalStorage({
            users: {
                user: store.getState().users.user,
            }
        })
    }

    return result;
};