import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { applyMiddleware, combineReducers, createStore, compose } from 'redux';
import commentReducer from './store/reducers/comment/commentReducer';
import commonReducer from './store/reducers/commonReducer'
import userReducer from './store/reducers/user/userReducer';
import {Provider} from "react-redux";
import thunk from 'redux-thunk';
import { loadFromLocalStorage, localStorageMiddleware } from './store/localStorage';
import doctorReducer from './store/reducers/doctorReducer';
import serviceReducer from './store/reducers/serviceReducer';
import accommodationReducer from './store/reducers/accommodationReducer';
import branchReducer from './store/reducers/branchReducer';
import {createBrowserHistory} from 'history';
import {ConnectedRouter, routerMiddleware, connectRouter} from 'connected-react-router';

const history = createBrowserHistory();

const reducers = combineReducers({
  router: connectRouter(history),
  comments: commentReducer,
  users: userReducer,
  commonReducer: commonReducer,
  doctors: doctorReducer,
  services: serviceReducer,
  accommadations: accommodationReducer,
  branches: branchReducer
})

const middleware = [
  routerMiddleware(history),
  thunk,
  localStorageMiddleware,
]


const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const store = createStore(reducers, loadFromLocalStorage(), composeEnhancers(applyMiddleware(...middleware)));

ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
        <>
          <App />
        </>
    </ConnectedRouter>
  </Provider>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
